# foggle = feature + toggle

- Track name: Backend
- Challenge name: Master of Puppets
- Position: Backend Dev + DevOps

# Reasons behind technical choices

- Rust programming language:
  - Type safety
  - High expressiveness
  - Declarative (at times)
  - Large ecosystem of libraries
  - Mature build tooling
  - Easy to deploy a static binary
  - And most importantly, I love it
- Relational DB:
  - Simple and easy to get started
  - Originally I used PostgreSQL:
    - A lot of features in case we might need them in the future
    - Easy to run in a container
  - Later I switched to using SQLite:
    - Behaves as an in-application DB
    - Easier to manage
    - Single file DB
    - Tests don't require external DB service
    - Of course, a "real" production environment would still keep using
      PostgreSQL as can be seen in the conditionally compiled versions
    - Good enough for the needs of this small application with replicas=1
- Schema:
  - No real surprises here

```
  +------+   +---------+
  | User +-->+ Project |
  +------+   +----+----+
                  |
            +-----+-----+
            |           |
       +----+---+   +---+----+
       | ApiKey |   | Toggle |
       +--------+   +--------+
```

- Architecture & Types:
  - I could spend weeks trying to come up with the best abstraction for a given problem
  - This type architecture (Rust types) could be improved a lot, I'm sure, but it works pretty well
  - Most of the stuff is using types from rocket (server framework) and diesel (ORM framework)
  - I want to create a new type for a Response that could be more tailored for this application and could be used more ergonomically, but I can't seem to find the right abstraction
  - Auth structure is, I think, a pretty good abstraction for our use case
- Testing
  - Didn't really see a point in unit testing this app since its pretty much just API on top of a database
  - Virtually no business logic to test
  - Functional testing done using postman

# Trade-offs

## Frontend

Who even needs one?...

# Documentation

OpenAPI 3.0 spec documenting the API can be found in file `openapi.yaml`.
Load it into [Swagger Editor](https://editor.swagger.io/) for your convenience.

[GitLab Pages](https://petoknm.gitlab.io/foggle/foggle) host the in-code
documentation for this project. This documentation includes private items and
items generated using macros, so the code doesn't contain all that crap.

You can also try out the API from Postman, by importing the file
`postman/Foggle.postman_collection.json`. This file is used for testing the API
during tests, so it isn't as nice as the OpenAPI one, but contains use-cases
that are tested.

# Deployment

Application gets built and packaged into a `debian:stable-slim` image using the
included `Dockerfile`. The resulting image then gets pushed into GitLab's image
registry.

[Deployed to k8s](https://foggle.petoknm.tk) on DigitalOcean (because I already
had an account with some money in there). Kubernetes manifests are located in
the `k8s` folder.

# Usage in CI

One possible way of reading values in a CI environment is to export them as
environment variables. Name of the environment variable is the same as the
feature toggle name and values are either 1 or 0.

```shell
# To export all toggles in a project
eval $(curl -H "X-ApiKey: $API_KEY" "$FOGGLE_URL/project/$PROJECT_ID/toggle?bash")

# To export only a specific toggle (NEW_FEATURE) in a project
eval $(curl -H "X-ApiKey: $API_KEY" "$FOGGLE_URL/project/$PROJECT_ID/toggle?bash" | grep NEW_FEATURE)
```

## GitLab CI

```yaml
variables:
  FOGGLE_URL: url_to_ur_favorite_feature_toggling_service
  API_KEY: your_api_key_here
  PROJECT_ID: your_project_id

before_script:
  - eval $(curl -H "X-ApiKey: $API_KEY" "$FOGGLE_URL/project/$PROJECT_ID/toggle?bash")
```

## Travis CI

```yaml
env:
  - FOGGLE_URL=url_to_ur_favorite_feature_toggling_service
  - API_KEY=your_api_key_here
  - PROJECT_ID=your_project_id

before_script:
  - eval $(curl -H "X-ApiKey: $API_KEY" "$FOGGLE_URL/project/$PROJECT_ID/toggle?bash")
```

## Circle CI

```yaml
jobs:
  <job>:
    env:
      FOGGLE_URL: url_to_ur_favorite_feature_toggling_service
      API_KEY: your_api_key_here
      PROJECT_ID: your_project_id
    steps:
      - run: eval $(curl -H "X-ApiKey: $API_KEY" "$FOGGLE_URL/project/$PROJECT_ID/toggle?bash")
```
