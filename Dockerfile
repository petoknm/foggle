FROM liuchong/rustup:nightly as builder

ADD . /build/
WORKDIR /build

RUN apt-get update && \
    apt-get install -y libsqlite3-dev sqlite3 && \
    cargo build --release && \
    cat migrations/*/up.sql | sqlite3 main.sqlite

FROM debian:stable-slim

RUN apt-get update && \
    apt-get install -y libsqlite3-0 && \
    apt-get clean

COPY --from=builder /build/target/release/foggle /usr/bin/foggle
COPY --from=builder /build/main.sqlite /root/main.sqlite
COPY --from=builder /build/Rocket.toml /root/Rocket.toml

EXPOSE 8000
WORKDIR /root
CMD ["/usr/bin/foggle"]
