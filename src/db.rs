use crate::models::*;
use crate::MainDbConn;
use diesel::prelude::*;
use diesel::result::Error as DieselError;
use diesel::sql_types::*;

pub struct Database(pub MainDbConn);

#[derive(Debug)]
pub struct Error(pub DieselError);

use rocket::http::Status;

impl From<Error> for Status {
    fn from(e: Error) -> Status {
        match e.0 {
            DieselError::NotFound => Status::NotFound,
            _ => Status::InternalServerError,
        }
    }
}

type QueryResult<T> = Result<T, Error>;

/// Maps DieselError into our Error
macro_rules! map {
    ($e:expr) => {$e.map_err(Error)};
}

/// Takes a DieselResult<Vec<_>> and returns the first element or Error(DieselError::NotFound) as QueryResult<_>
macro_rules! first {
    ($e:expr) => {map!($e.and_then(|mut v| v.drain(..).nth(0).ok_or(DieselError::NotFound)))};
}

/// Takes a DieselResult<usize>, usually result of `query.execute()`, and checks if at least one
/// row was affected and returns QueryResult<()>
macro_rules! check {
    ($e:expr) => {map!($e.and_then(|c| if c == 0 { Err(DieselError::NotFound) } else { Ok(()) }))};
}

impl Database {
    pub fn create_user(&self, username: String, password: String) -> QueryResult<()> {
        let password = bcrypt::hash(&password, 12).unwrap();
        check!(
            diesel::sql_query("INSERT INTO users(username, password) VALUES (?, ?)")
                .bind::<VarChar, _>(username)
                .bind::<VarChar, _>(password)
                .execute(&*self.0)
        )
    }

    pub fn find_user(&self, id: i32) -> QueryResult<User> {
        first!(diesel::sql_query("SELECT * FROM users WHERE id = ?")
            .bind::<Integer, _>(id)
            .load(&*self.0))
    }

    pub fn find_user_by_username(&self, username: &str) -> QueryResult<User> {
        first!(
            diesel::sql_query("SELECT * FROM users WHERE username = ? LIMIT 1")
                .bind::<VarChar, _>(username)
                .load(&*self.0)
        )
    }

    pub fn delete_user(&self, id: i32) -> QueryResult<()> {
        check!(diesel::sql_query("DELETE FROM users WHERE id = ?")
            .bind::<Integer, _>(id)
            .execute(&*self.0))
    }

    pub fn create_project(&self, user_id: i32, name: String) -> QueryResult<()> {
        check!(diesel::sql_query(
            "INSERT INTO projects(user_id, name, last_accessed_on) VALUES (?, ?, strftime('%s','now'))"
        )
        .bind::<Integer, _>(user_id)
        .bind::<VarChar, _>(name)
        .execute(&*self.0))
    }

    fn update_project_access_time(&self, project_id: i32, user_id: i32) -> QueryResult<()> {
        check!(diesel::sql_query(
            r#"
            UPDATE projects SET last_accessed_on = strftime('%s','now')
            WHERE id IN (
                SELECT p.id FROM projects p
                INNER JOIN users u ON u.id = p.user_id
                WHERE p.id = ? AND u.id = ?
            )
            "#,
        )
        .bind::<Integer, _>(project_id)
        .bind::<Integer, _>(user_id)
        .execute(&*self.0))
    }

    pub fn cleanup(&self) -> QueryResult<usize> {
        map!(diesel::sql_query(
            "DELETE FROM projects WHERE last_accessed_on < strftime('%s','now','-1 month')",
        )
        .execute(&*self.0))
    }

    pub fn find_project_with_user_id(&self, project_id: i32, user_id: i32) -> QueryResult<Project> {
        self.update_project_access_time(project_id, user_id)?;
        first!(diesel::sql_query(
            r#"
                SELECT p.* FROM projects p
                INNER JOIN users u ON u.id = p.user_id
                WHERE p.id = ? AND u.id = ?
            "#
        )
        .bind::<Integer, _>(project_id)
        .bind::<Integer, _>(user_id)
        .load(&*self.0))
    }

    pub fn find_projects_with_user_id(&self, user_id: i32) -> QueryResult<Vec<Project>> {
        // Update projects' access times
        map!(diesel::sql_query(
            r#"
            UPDATE projects SET last_accessed_on = strftime('%s','now')
            WHERE id IN (
                SELECT p.id FROM projects p
                INNER JOIN users u ON u.id = p.user_id
                WHERE u.id = ?
            )
        "#,
        )
        .bind::<Integer, _>(user_id)
        .execute(&*self.0))?;

        map!(diesel::sql_query(
            r#"
            SELECT p.* FROM projects p
            INNER JOIN users u ON u.id = p.user_id
            WHERE u.id = ?
        "#,
        )
        .bind::<Integer, _>(user_id)
        .load(&*self.0))
    }

    pub fn delete_project_with_user_id(&self, project_id: i32, user_id: i32) -> QueryResult<()> {
        check!(diesel::sql_query(
            r#"
            DELETE FROM projects
            WHERE id IN (
                SELECT p.id FROM projects p
                INNER JOIN users u ON u.id = p.user_id
                WHERE p.id = ? AND u.id = ?
            )
        "#,
        )
        .bind::<Integer, _>(project_id)
        .bind::<Integer, _>(user_id)
        .execute(&*self.0))
    }

    pub fn create_api_key(
        &self,
        user_id: i32,
        project_id: i32,
        key: String,
        can_read: bool,
        can_write: bool,
    ) -> QueryResult<()> {
        // Check project owner
        let _project = self.find_project_with_user_id(project_id, user_id)?;

        check!(diesel::sql_query(
            "INSERT INTO api_keys(project_id, key, can_read, can_write) VALUES (?, ?, ?, ?)",
        )
        .bind::<Integer, _>(project_id)
        .bind::<VarChar, _>(key)
        .bind::<Bool, _>(can_read)
        .bind::<Bool, _>(can_write)
        .execute(&*self.0))
    }

    pub fn find_api_keys_with_user_id_project_id(
        &self,
        user_id: i32,
        project_id: i32,
    ) -> QueryResult<Vec<ApiKey>> {
        self.update_project_access_time(project_id, user_id)?;
        map!(diesel::sql_query(
            r#"
            SELECT ak.* FROM api_keys ak
            INNER JOIN projects p ON p.id = ak.project_id
            INNER JOIN users u ON u.id = p.user_id
            WHERE u.id = ? AND p.id = ?
        "#,
        )
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .load(&*self.0))
    }

    pub fn find_user_project_api_key(&self, api_key: &str) -> QueryResult<(User, Project, ApiKey)> {
        let (user, project, api_key): (User, Project, ApiKey) = first!(diesel::sql_query(
            r#"
                SELECT * FROM api_keys ak
                INNER JOIN projects p ON p.id = ak.project_id
                INNER JOIN users u ON u.id = p.user_id
                WHERE ak.key = ?
                LIMIT 1
            "#
        )
        .bind::<VarChar, _>(api_key)
        .load(&*self.0))?;

        self.update_project_access_time(project.id, user.id)?;

        Ok((user, project, api_key))
    }

    pub fn delete_api_key_with_user_id_project_id(
        &self,
        api_key: &str,
        user_id: i32,
        project_id: i32,
    ) -> QueryResult<()> {
        self.update_project_access_time(project_id, user_id)?;
        check!(diesel::sql_query(
            r#"
            DELETE FROM api_keys
            WHERE id IN (
                SELECT ak.id FROM api_keys ak
                INNER JOIN projects p ON p.id = ak.project_id
                INNER JOIN users u ON u.id = p.user_id
                WHERE ak.key = ? AND u.id = ? AND p.id = ?
            )
            "#,
        )
        .bind::<VarChar, _>(api_key)
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .execute(&*self.0))
    }

    pub fn create_toggle(
        &self,
        user_id: i32,
        project_id: i32,
        name: String,
        value: bool,
    ) -> QueryResult<()> {
        // Check project owner
        let _project = self.find_project_with_user_id(project_id, user_id)?;

        check!(
            diesel::sql_query("INSERT INTO toggles(project_id, name, value) VALUES (?, ?, ?)")
                .bind::<Integer, _>(project_id)
                .bind::<VarChar, _>(name)
                .bind::<Bool, _>(value)
                .execute(&*self.0)
        )
    }

    pub fn find_toggle_with_user_id_project_id(
        &self,
        toggle_id: i32,
        user_id: i32,
        project_id: i32,
    ) -> QueryResult<Toggle> {
        self.update_project_access_time(project_id, user_id)?;
        first!(diesel::sql_query(
            r#"
                SELECT t.* FROM toggles t
                INNER JOIN projects p ON p.id = t.project_id
                INNER JOIN users u ON u.id = p.user_id
                WHERE t.id = ? AND u.id = ? AND p.id = ?
            "#
        )
        .bind::<Integer, _>(toggle_id)
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .load(&*self.0))
    }

    pub fn find_toggles_with_user_id_project_id(
        &self,
        user_id: i32,
        project_id: i32,
    ) -> QueryResult<Vec<Toggle>> {
        self.update_project_access_time(project_id, user_id)?;
        map!(diesel::sql_query(
            r#"
            SELECT t.* FROM toggles t
            INNER JOIN projects p ON p.id = t.project_id
            INNER JOIN users u ON u.id = p.user_id
            WHERE u.id = ? AND p.id = ?
        "#,
        )
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .load(&*self.0))
    }

    pub fn update_toggle_with_user_id_project_id(
        &self,
        toggle_id: i32,
        user_id: i32,
        project_id: i32,
        value: bool,
    ) -> QueryResult<()> {
        self.update_project_access_time(project_id, user_id)?;
        check!(diesel::sql_query(
            r#"
            UPDATE toggles
            SET value = ?
            WHERE id IN (
                SELECT t.id FROM toggles t
                INNER JOIN projects p ON p.id = t.project_id
                INNER JOIN users u ON u.id = p.user_id
                WHERE t.id = ? AND u.id = ? AND p.id = ?
            )
            "#,
        )
        .bind::<Bool, _>(value)
        .bind::<Integer, _>(toggle_id)
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .execute(&*self.0))
    }

    pub fn delete_toggle_with_user_id_project_id(
        &self,
        toggle_id: i32,
        user_id: i32,
        project_id: i32,
    ) -> QueryResult<()> {
        self.update_project_access_time(project_id, user_id)?;
        check!(diesel::sql_query(
            r#"
            DELETE FROM toggles
            WHERE id IN (
                SELECT t.id FROM toggles t
                INNER JOIN projects p ON p.id = t.project_id
                INNER JOIN users u ON u.id = p.user_id
                WHERE t.id = ? AND u.id = ? AND p.id = ?
            )
            "#,
        )
        .bind::<Integer, _>(toggle_id)
        .bind::<Integer, _>(user_id)
        .bind::<Integer, _>(project_id)
        .execute(&*self.0))
    }
}
