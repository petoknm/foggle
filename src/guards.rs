use crate::db::Database;
use crate::models::*;
use crate::MainDbConn;
use rocket::http::Status;
use rocket::outcome::IntoOutcome;
use rocket::request::{FromRequest, Outcome, Request};
use std::marker::PhantomData;

/// Create an instance of our Database.
impl<'a, 'r> FromRequest<'a, 'r> for Database {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> Outcome<Database, ()> {
        let conn = request.guard::<MainDbConn>()?;
        Outcome::Success(Database(conn))
    }
}

/// Permission trait.
///
/// Permissions has to decide whether a given auth structure is allowed or not.
///
/// # Example
/// ```rust
/// impl Permission for Read {
///     fn is_allowed(auth: &Auth<Read>) -> bool {
///         auth.can_read()
///     }
/// }
/// ```
pub trait Permission: Sized {
    fn is_allowed(auth: &Auth<Self>) -> bool;
}

pub struct Read;
pub struct Write;
pub struct ReadWrite;

impl Permission for Read {
    fn is_allowed(auth: &Auth<Read>) -> bool {
        auth.can_read()
    }
}

impl Permission for Write {
    fn is_allowed(auth: &Auth<Write>) -> bool {
        auth.can_write()
    }
}

impl Permission for ReadWrite {
    fn is_allowed(auth: &Auth<ReadWrite>) -> bool {
        auth.can_read() && auth.can_write()
    }
}

/// Our main auth structure.
///
/// Request needs to be authenticated to create this structure. If the request uses cookies for
/// auth, this structure will contain only the User object. If the request uses an API key, this
/// structure will contain the API key and its associated Project and User.
///
/// # Usage
/// ```rust
/// #[get("/precious_resource")]
/// fn get_precious_resource(auth: Auth<Read>) -> Response<PreciousResource> {
///     Ok(fetch_precious_resource(auth.user.id))
/// }
/// ```
pub struct Auth<P: Permission> {
    pub user: User,
    pub project: Option<Project>,
    pub api_key: Option<ApiKey>,
    pd: PhantomData<P>,
}

impl<P: Permission> Auth<P> {
    fn new(request: &Request) -> Option<Auth<P>> {
        let db = request.guard::<Database>().succeeded()?;

        // Look for user ID cookie
        let user_id_cookie = request.cookies().get_private("user_id");
        if let Some(cookie) = user_id_cookie {
            let id = cookie.value().parse().ok()?;
            let user = db.find_user(id).ok()?;
            return Some(Auth {
                user,
                project: None,
                api_key: None,
                pd: PhantomData,
            });
        }

        // Look for API key header
        let api_key = request.headers().get_one("X-ApiKey")?;
        let (user, project, api_key) = db.find_user_project_api_key(api_key).ok()?;

        Some(Auth {
            user,
            project: Some(project),
            api_key: Some(api_key),
            pd: PhantomData,
        })
    }

    fn can_read(&self) -> bool {
        match &self.api_key {
            None => true,
            Some(api_key) => api_key.can_read,
        }
    }

    fn can_write(&self) -> bool {
        match &self.api_key {
            None => true,
            Some(api_key) => api_key.can_write,
        }
    }
}

impl<'a, 'r, P: Permission> FromRequest<'a, 'r> for Auth<P> {
    type Error = ();

    fn from_request(request: &'a Request<'r>) -> Outcome<Auth<P>, ()> {
        Auth::new(request)
            .and_then(|auth| match P::is_allowed(&auth) {
                true => Some(auth),
                false => None,
            })
            .into_outcome((Status::Unauthorized, ()))
    }
}
