table! {
    api_keys (id) {
        id -> Integer,
        project_id -> Integer,
        key -> Text,
        can_read -> Bool,
        can_write -> Bool,
    }
}

table! {
    projects (id) {
        id -> Integer,
        user_id -> Integer,
        name -> Text,
        last_accessed_on -> BigInt,
    }
}

table! {
    toggles (id) {
        id -> Integer,
        project_id -> Integer,
        name -> Text,
        value -> Bool,
    }
}

table! {
    users (id) {
        id -> Integer,
        username -> Text,
        password -> Text,
    }
}

joinable!(api_keys -> projects (project_id));
joinable!(projects -> users (user_id));
joinable!(toggles -> projects (project_id));

allow_tables_to_appear_in_same_query!(api_keys, projects, toggles, users,);
