#![feature(proc_macro_hygiene, decl_macro, type_alias_enum_variants)]

#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;
extern crate rocket_cors;
#[macro_use]
extern crate diesel;
extern crate base64;
extern crate bcrypt;
extern crate rand;
extern crate serde;

mod db;
mod guards;
mod models;
mod routes;
mod schema;

use crate::routes::*;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::SqliteConnection;
use rocket_cors::CorsOptions;

#[database("main")]
pub struct MainDbConn(SqliteConnection);

type Response<T> = Result<T, rocket::http::Status>;

fn main() {
    std::thread::spawn(|| loop {
        let manager = ConnectionManager::<SqliteConnection>::new("main.sqlite");
        let pool = Pool::new(manager).unwrap();
        let db = db::Database(MainDbConn(pool.get().unwrap()));
        let res = db.cleanup();
        eprintln!("cleanup result: {:?}", res);
        std::thread::sleep(std::time::Duration::from_secs(60 * 60));
    });

    let cors = CorsOptions::default().to_cors().unwrap();

    rocket::ignite()
        .attach(MainDbConn::fairing())
        .attach(cors)
        .mount("/", routes![
            login, logout,
            create_user, get_user, delete_user,
            create_project, get_project, get_projects, delete_project,
            create_api_key, get_api_keys, delete_api_key,
            create_toggle, get_toggle, get_toggles, update_toggle, delete_toggle,
            export_toggles_bash
        ])
        .launch();
}
