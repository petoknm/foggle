use crate::db::Database;
use crate::guards::*;
use crate::models::*;
use crate::Response;
use rocket_contrib::json::Json;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct CreateToggleReq {
    name: String,
    value: bool,
}

#[post("/project/<project_id>/toggle", data = "<req>")]
pub fn create_toggle(
    db: Database,
    auth: Auth<ReadWrite>,
    project_id: i32,
    req: Json<CreateToggleReq>,
) -> Response<()> {
    db.create_toggle(auth.user.id, project_id, req.0.name, req.0.value)?;
    Ok(())
}

#[get("/project/<project_id>/toggle?bash")]
pub fn export_toggles_bash(db: Database, auth: Auth<Read>, project_id: i32) -> Response<String> {
    let toggles = db.find_toggles_with_user_id_project_id(auth.user.id, project_id)?;

    let s = toggles
        .iter()
        .map(|t| format!("export {}={}\n", t.name, if t.value { 1 } else { 0 }))
        .collect();

    Ok(s)
}

#[get("/project/<project_id>/toggle")]
pub fn get_toggles(db: Database, auth: Auth<Read>, project_id: i32) -> Response<Json<Vec<Toggle>>> {
    let toggles = db
        .find_toggles_with_user_id_project_id(auth.user.id, project_id)
        .unwrap();
    Ok(Json(toggles))
}

#[get("/project/<project_id>/toggle/<toggle_id>")]
pub fn get_toggle(
    db: Database,
    auth: Auth<Read>,
    toggle_id: i32,
    project_id: i32,
) -> Response<Json<Toggle>> {
    let toggle = db
        .find_toggle_with_user_id_project_id(toggle_id, auth.user.id, project_id)
        .unwrap();
    Ok(Json(toggle))
}

#[derive(Deserialize, Debug)]
pub struct UpdateToggleReq {
    value: bool,
}

#[put("/project/<project_id>/toggle/<toggle_id>", data = "<req>")]
pub fn update_toggle(
    db: Database,
    auth: Auth<Write>,
    toggle_id: i32,
    project_id: i32,
    req: Json<UpdateToggleReq>,
) -> Response<()> {
    db.update_toggle_with_user_id_project_id(toggle_id, auth.user.id, project_id, req.0.value)
        .unwrap();
    Ok(())
}

#[delete("/project/<project_id>/toggle/<toggle_id>")]
pub fn delete_toggle(
    db: Database,
    auth: Auth<ReadWrite>,
    toggle_id: i32,
    project_id: i32,
) -> Response<()> {
    db.delete_toggle_with_user_id_project_id(toggle_id, auth.user.id, project_id)
        .unwrap();
    Ok(())
}
