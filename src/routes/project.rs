use crate::db::Database;
use crate::guards::*;
use crate::models::Project;
use crate::Response;
use rocket_contrib::json::Json;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct CreateProjectReq {
    name: String,
}

#[post("/project", data = "<req>")]
pub fn create_project(
    db: Database,
    auth: Auth<ReadWrite>,
    req: Json<CreateProjectReq>,
) -> Response<()> {
    db.create_project(auth.user.id, req.0.name)?;
    Ok(())
}

#[get("/project")]
pub fn get_projects(db: Database, auth: Auth<Read>) -> Response<Json<Vec<Project>>> {
    let projects = db.find_projects_with_user_id(auth.user.id)?;
    Ok(Json(projects))
}

#[get("/project/<project_id>")]
pub fn get_project(db: Database, auth: Auth<Read>, project_id: i32) -> Response<Json<Project>> {
    let project = db.find_project_with_user_id(project_id, auth.user.id)?;
    Ok(Json(project))
}

#[delete("/project/<project_id>")]
pub fn delete_project(db: Database, auth: Auth<Read>, project_id: i32) -> Response<()> {
    db.delete_project_with_user_id(project_id, auth.user.id)?;
    Ok(())
}
