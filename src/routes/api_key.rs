use crate::db::Database;
use crate::guards::*;
use crate::models::*;
use crate::Response;
use base64::{CharacterSet, Config};
use rand::Rng;
use rocket_contrib::json::Json;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct CreateApiKeyReq {
    can_read: bool,
    can_write: bool,
}

#[post("/project/<project_id>/api_key", data = "<req>")]
pub fn create_api_key(
    db: Database,
    auth: Auth<ReadWrite>,
    project_id: i32,
    req: Json<CreateApiKeyReq>,
) -> Response<()> {
    let mut key = [0; 64];
    rand::thread_rng().fill(&mut key);
    let config = Config::new(CharacterSet::UrlSafe, true);
    let key = base64::encode_config(&key[..], config);
    db.create_api_key(
        auth.user.id,
        project_id,
        key,
        req.0.can_read,
        req.0.can_write,
    )?;
    Ok(())
}

#[get("/project/<project_id>/api_key")]
pub fn get_api_keys(
    db: Database,
    auth: Auth<Read>,
    project_id: i32,
) -> Response<Json<Vec<ApiKey>>> {
    let api_keys = db.find_api_keys_with_user_id_project_id(auth.user.id, project_id)?;
    Ok(Json(api_keys))
}

#[delete("/project/<project_id>/api_key/<api_key>")]
pub fn delete_api_key(
    db: Database,
    auth: Auth<ReadWrite>,
    project_id: i32,
    api_key: String,
) -> Response<()> {
    db.delete_api_key_with_user_id_project_id(&api_key, auth.user.id, project_id)?;
    Ok(())
}
