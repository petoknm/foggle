use crate::db::Database;
use crate::Response;
use rocket::http::{Cookie, Cookies, Status};
use rocket_contrib::json::Json;
use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub struct LoginReq {
    username: String,
    password: String,
}

#[post("/login", data = "<req>")]
pub fn login(db: Database, mut cookies: Cookies, req: Json<LoginReq>) -> Response<()> {
    let user = db.find_user_by_username(&req.0.username)?;
    let correct_password = bcrypt::verify(req.0.password, &user.password).unwrap();

    if correct_password {
        let cookie = Cookie::new("user_id", user.id.to_string());
        cookies.add_private(cookie);
        Ok(())
    } else {
        Err(Status::Unauthorized)
    }
}

#[post("/logout")]
pub fn logout(mut cookies: Cookies) -> Response<()> {
    cookies.remove_private(Cookie::named("user_id"));
    Ok(())
}
