use crate::db::Database;
use crate::guards::*;
use crate::models::User;
use crate::Response;
use rocket_contrib::json::Json;
use serde::Deserialize;

#[derive(Deserialize)]
pub struct CreateUserReq {
    pub username: String,
    pub password: String,
}

#[post("/user", data = "<req>")]
pub fn create_user(db: Database, req: Json<CreateUserReq>) -> Response<()> {
    db.create_user(req.0.username, req.0.password)?;
    Ok(())
}

#[get("/user")]
pub fn get_user(auth: Auth<Read>) -> Response<Json<User>> {
    Ok(Json(auth.user))
}

#[delete("/user")]
pub fn delete_user(db: Database, auth: Auth<ReadWrite>) -> Response<()> {
    db.delete_user(auth.user.id)?;
    Ok(())
}
