use crate::schema::toggles;
use serde::Serialize;

#[derive(QueryableByName, Serialize, Debug)]
#[table_name = "toggles"]
pub struct Toggle {
    pub id: i32,
    pub project_id: i32,
    pub name: String,
    pub value: bool,
}
