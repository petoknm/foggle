use crate::schema::users;
use serde::Serialize;

#[derive(QueryableByName, Serialize, Debug)]
#[table_name = "users"]
pub struct User {
    pub id: i32,
    pub username: String,
    pub password: String,
}
