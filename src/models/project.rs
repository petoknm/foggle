use crate::schema::projects;
use serde::Serialize;

#[derive(QueryableByName, Serialize, Debug)]
#[table_name = "projects"]
pub struct Project {
    pub id: i32,
    pub user_id: i32,
    pub name: String,
    pub last_accessed_on: i64,
}
