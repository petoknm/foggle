use crate::schema::api_keys;
use serde::Serialize;

#[derive(QueryableByName, Serialize, Debug)]
#[table_name = "api_keys"]
pub struct ApiKey {
    pub id: i32,
    pub project_id: i32,
    pub key: String,
    pub can_read: bool,
    pub can_write: bool,
}
