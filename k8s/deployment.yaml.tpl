apiVersion: apps/v1
kind: Deployment
metadata:
  name: foggle
  labels:
    app: foggle
spec:
  replicas: 1
  selector:
    matchLabels:
      app: foggle
  template:
    metadata:
      labels:
        app: foggle
    spec:
      containers:
      - name: foggle
        image: $IMAGE
        ports:
        - containerPort: 8000
